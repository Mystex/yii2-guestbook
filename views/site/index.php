<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Гостевая книга';
?>

<div class="site-index">

    <div class="jumbotron">
        <h1>Гостевая книга</h1>

        <p>Оставьте своё сообщение</p>


        <?php if (Yii::$app->session->hasFlash('addFormSubmitted')): ?>
            <div class="alert alert-success">
                Ваше сообщение добавлено.
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->hasFlash('addFormError')): ?>
            <div class="alert alert-danger">
                Произошла ошибка.
            </div>
        <?php endif; ?>


        <?php $form = ActiveForm::begin([
            'id' => 'add-form',
            'enableAjaxValidation' => true
        ]); ?>

            <?= $form->field($model, 'name', ['inputOptions'=>['class'=>'form-control','placeholder'=>'Имя']])->label(false); ?>
            <?= $form->field($model, 'email', ['inputOptions'=>['class'=>'form-control','placeholder'=>'E-mail']])->label(false) ?>
            <?= $form->field($model, 'body', ['inputOptions'=>['class'=>'form-control','placeholder'=>'Сообщение']])->textArea(['rows' => 6])->label(false) ?>
            <?php /*= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
            ])*/ ?>
            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-default', 'name' => 'add-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>


    </div>

    <div class="body-content" id="items">

        <?php foreach ($data as $i => $post): ?>

            <div class="row" id="id-<?php echo $post->id; ?>">

                <p><?php echo $post->body; ?></p>

                <p style="color:gray">#<?php echo $post->id; ?> Написал <strong><?php echo $post->name; ?></strong> <?php echo $post->created; ?></p>

            </div>

            <hr>
        <?php endforeach; ?>


    </div>

    <div class="row">
        <nav>
            <ul class="pagination" id="pagination">
                <?php for ($i = 1; $i <= $pages; $i++) { ?>
                    <li<?php if($i == $page) print ' class="active"';?> data-id="<?=$i?>"><a href="/site/<?=$i?>"><?=$i?></a></li>
                <?php } ?>
            </ul>
        </nav>
    </div>
</div>
