<?php

namespace app\controllers;

use app\models\Guestbook;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class SiteController extends Controller
{
    // Количество записей на странице
    private $itemsPerPage = 10;

    // action по-умолчанию
    public $defaultAction = 'page';

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Главная страница
     */
    public function actionIndex()
    {
        $this->actionPage(1);
    }

    /**
     * Страница с записями гостевой книги
     */
    public function actionPage($page = 1)
    {
        $model = new Guestbook();

        // AJAX-валидация
        if (Yii::$app->request->isAjax
            && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        }

        // Количество записей
        $count = $model->find()->count();

        // Количество страниц
        $pages = ceil ($count / $this->itemsPerPage);

        // Существует ли страница с таким номером
        if ($page > $pages) {
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }

        $request = Yii::$app->request;

        // Если пришёл POST-запрос и прошла валидация
        if ($model->load($request->post()) && $model->validate()) {

            // Дата добавления
            $model->created = date('y-m-j h:i:s');

            // Если запись добавлена
            if ($model->save()) {
                Yii::$app->session->setFlash('addFormSubmitted');
                return $this->redirect('/index.php?r=site/page#id-' . $model->id);
            } else {
                Yii::$app->session->setFlash('addFormError');
            }

            // Обновление страницы
            return $this->refresh();
        }


        $data = $model->find()
            ->orderBy('id desc')
            ->offset(($page - 1) * $this->itemsPerPage)
            ->limit($this->itemsPerPage)
            ->all();

        return $this->render('index', array(
            'page' => $page,
            'count' => $count,
            'pages' => $pages,
            'data' => $data,
            'model' => $model
        ));
    }

    public function actionAjaxPage($page = 1)
    {
        $model = new Guestbook();

        if (!Yii::$app->request->isAjax) {
            return false;
        }

        // Количество записей
        $count = $model->find()->count();

        // Количество страниц
        $pages = ceil ($count / $this->itemsPerPage);

        // Существует ли страница с таким номером
        if ($page > $pages) {
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }

        // Записи
        $data = $model->find()
            ->orderBy('id desc')
            ->offset(($page - 1) * $this->itemsPerPage)
            ->limit($this->itemsPerPage)
            ->all();

        // Ответ клиенту
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = [
            'page' => $page,
            'count' => $count,
            'pages' => $pages,
            'data' => $data,
            'model' => $model
        ];

        return $response->send();
    }
}
