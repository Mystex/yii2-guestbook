$(document).ready(function() {
    $('.pagination > li > a').click(function (e) {
        var page = $(this).parent().attr('data-id');
        var obj = $(this).parent();

        $.ajax({
            url: '/site/ajax-page',
            data: {
                page: page
            },
            success: function (data) {

                // Чистка контента
                $('#items').empty();

                // Пагинация
                $('#pagination li').removeClass('active');
                obj.addClass('active');

                // Строка url в браузере
                var redirect = 'site/' + page;
                history.pushState('', '', redirect);

                // Вывод записей
                for (i = 0; i < data.data.length; i++) {
                    $('#items').append('<div class="row" id="id-' + data.data[i].id + '">'
                    + '<p>' + data.data[i].body + '</p>' + '<p style="color:gray">#' + data.data[i].id
                    + ' Написал <strong>' + data.data[i].name + '</strong> ' + data.data[i].created + '</p>'
                    + '</div><hr>');
                }

                // Плавная прокрутка
                destination = $('#items').offset().top;
                $('body').animate( { scrollTop: destination }, 1100 );
            }
        });
        return false;
    });
});